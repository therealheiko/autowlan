from arm32v6/alpine

# Install packages
RUN apk update && apk add hostapd iw dhcp vim iptables
RUN apk add openct pcscd pcsc-tools libpcsclite-dev libnl-dev

# Configure Hostapd (default will be open)
ADD confs/hostapd_confs/open.conf /etc/hostapd/hostapd.conf
# Configure DHCPD
ADD confs/dhcpd.conf /etc/dhcp/dhcpd.conf
RUN touch /var/lib/dhcp/dhcpd.leases

# Configure networking
ADD confs/interfaces /etc/network/interfaces
ADD confs/iptables.sh /iptables.sh
ADD confs/iptables_off.sh /iptables_off.sh

#EAP SIM
ADD confs/openct.conf /wpa_supplicant/openct.conf
ADD confs/wpa_supplicant_eap-sim.conf /wpa_supplicant/wpa_supplicant_eap-sim.conf
ADD confs/.config /wpa_supplicant/.config
ADD confs/openct.conf /etc/openct.conf 
RUN iwconfig wlan0 essid "FreeWifi_secure" && ./wpa_supplicant -i wlan0 -c wpa_supplicant_eap-sim.conf

# Copy and execute init file
ADD confs/start.sh /start.sh
CMD ["/bin/sh", "/start.sh"]
